%{
  open Expr
%}

%token EOF

%token <string> VAR
%token <string> STRING
%token <int> INT

%token OR
%token AND
%token NOT
%token <string> BIN_CMP
%token <string> BIN_MULT
%token <string> BIN_PLUS
%token MINUS

%token UNDERSCORE
%token IMPL
%token BAR
%token CASE OF

%token LET EQUALS IN

%token LPAR RPAR

%nonassoc LPAR RPAR
%nonassoc LET IN
%nonassoc BAR
%nonassoc CASE OF
%nonassoc IMPL
%left OR
%left AND
%nonassoc NOT
%left BIN_PLUS MINUS
%nonassoc UMINUS
%left BIN_MULT
%left BIN_CMP EQUALS
%nonassoc VAR
/* Les non-terminaux par lesquels l'analyse peut commencer,
 * et la donnée de leurs types. */

%start terminated_expr
%type <Expr.t> terminated_expr

%%

terminated_expr:
  | expr EOF { $1 }

expr:
  | INT                            { Int $1 }
  | VAR                            { Var $1 }
  | STRING                         { String $1 }
  | UNDERSCORE                     { String "_" }
  | LPAR expr RPAR                 { $2 }
  | LET VAR EQUALS expr IN expr    { Let ($2,$4,$6) }
  | CASE expr OF pmatch            { Case ($2,$4)}
  | expr EQUALS expr               { App ("=",[$1;$3]) }
  | expr IMPL expr                 { App ("=>",[$1;$3]) }
  | MINUS expr %prec UMINUS        { App ("-", [$2]) }
  | expr MINUS expr                { App ("-", [$1;$3])}
  | expr BIN_PLUS expr             { App ($2,[$1;$3]) }
  | expr BIN_MULT expr             { App ($2,[$1;$3]) }
  | expr BIN_CMP expr              { App ($2,[$1;$3]) }
  | expr AND expr                  { App ("and",[$1;$3]) }
  | expr OR expr                   { App ("or",[$1;$3]) }
  | NOT expr                       { App ("not",[$2]) }

pmatch:
  | prule                          { [$1] }
  | pmatch BAR prule               { $1@[$3] }

prule:
  | INT IMPL expr                  { Int $1, $3 }
  | STRING IMPL expr               { String $1, $3 }
  | VAR IMPL expr                  { Var $1, $3 }
  | UNDERSCORE IMPL expr           { Any, $3 }
