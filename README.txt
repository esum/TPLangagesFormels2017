	Compte rendu du TP de langages formels
	
	Benjamin Graillot

	J'ai défini les expressions régulières pour les différents lexèmes et j'ai écrit des fonctions pour les convertir vers les formats demandés.
	J'ai ajouté la gestion des sauts de ligne dans le buffer et les commentaires imbriqués.
	J'ai défini la précédence et l'associativité des tokens pour éliminer les conflits shift/reduce.
	J'ai implémenté le case et le moins unaire.
	J'ai ajouté la simplification d'AST : les opérations et comparaisons sur les entiers s'évaluent directement (une comparaison renvoyant 1 si elle est vérifiée et 0 sinon).
	Les AST contenant des variables libres sont également rejetés.
	
