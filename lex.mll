
{
  open Parse   (* ./parse.mly *)
  open Lexing  (* librairie standard *)

  (* Gère les échapements dans les strings *)
  let trim_escape s =
    let l = ref 0 in
    let i = ref 1 in
    while !i < (String.length s) - 1 do
      if !i <> (String.length s) - 2 then begin
        if s.[!i] = '\\' && s.[!i+1] = '"' then begin
          i := !i + 2 end
        else begin
          incr i end end
      else
        incr i;
      incr l
    done;
    let res = String.make (!l) ' ' in
    i := 0;
    let j = ref 1 in
    while !i < !l do
      if !j < (String.length s) - 2 then begin
        if s.[!j] = '\\' && s.[!j+1] = '"' then begin
          res.[!i] <- '"';
          j := !j + 2;
          incr i; end
        else begin
          res.[!i] <- s.[!j];
          incr i;
          incr j; end end
      else begin
        res.[!i] <- s.[!j];
        incr i;
        incr j end
    done;
    res

  (* Conversions des entiers décimaux et hexadécimaux en int *)
  let digit c = match c with
    | '0' | '1' | '2' | '3' | '4' | '5' | '6' | '7' | '8' | '9' -> (int_of_char c) - 48
    | 'a' | 'A' -> 10
    | 'b' | 'B' -> 11
    | 'c' | 'C' -> 12
    | 'd' | 'D' -> 13
    | 'e' | 'E' -> 14
    | 'f' | 'F' -> 15
    | _ -> 0
  let convert s =
    if String.length s > 2 then begin
      if s.[1] = 'x' || s.[1] = 'X' then begin
        let res = ref (digit s.[2]) in
        for i = 3 to (String.length s) - 1 do
          if s.[i] <> '_' then begin
            res := !res * 16;
            res := !res + digit s.[i]; end
        done;
        !res end
      else begin
        let res = ref (digit s.[0]) in
        for i = 1 to (String.length s) - 1 do
          if s.[i] <> '_' then begin
            res := !res * 10;
            res := !res + digit s.[i]; end
        done;
        !res end end
    else
      let res = ref (digit s.[0]) in
      for i = 1 to (String.length s) - 1 do
        if s.[i] <> '_' then begin
          res := !res * 10;
          res := !res + digit s.[i]; end
      done;
      !res
}

let varname = ['a'-'z' '_']['a'-'z' 'A'-'Z' '0'-'9' '_' '\'']*
let integer = ['0'-'9']['0'-'9' '_']* | "0"("x"|"X")['0'-'9' 'A'-'F' 'a'-'f' '_']+
let str = "\"" ([^ '"']|"\\\"")* "\""
let opsymbs = ['=' '<' '>' '^' '+' '-' '*' '/' '%' '!' '$' '?' '.' ':' ';']*
let bin_cmp = ['=' '<' '>' '^'] opsymbs
let bin_plus = ['+' '-'] opsymbs
let bin_mult = ['*' '/' '%'] opsymbs

rule token = parse
  
  | "(*"               { comment 0 lexbuf }

  | "let"              { LET }
  | "="                { EQUALS }
  | "in"               { IN }

  | "case"             { CASE }
  | "of"               { OF }
  | "|"                { BAR }
  | "=>"               { IMPL }
  | "_"                { UNDERSCORE }

  | "("                { LPAR }
  | ")"                { RPAR }

  | "and"              { AND }
  | "or"               { OR }
  | "not"              { NOT }
  | "-"                { MINUS }
  | bin_plus as op     { BIN_PLUS op }
  | bin_mult as op     { BIN_MULT op }
  | bin_cmp as op      { BIN_CMP op }

  | str as s           { STRING (trim_escape s) }
  | integer as n       { INT (convert n) }

  | varname as v       { VAR v }

  | [' ' '\t' '\r']+   { token lexbuf }
  (* gestion des sauts de ligne *)
  | '\n'               { lexbuf.lex_curr_p <- {pos_fname = lexbuf.lex_curr_p.pos_fname ; pos_lnum = 1 + lexbuf.lex_curr_p.pos_lnum ; pos_bol =  lexbuf.lex_curr_p.pos_cnum ; pos_cnum = 0 } ; token lexbuf }

  | eof                { EOF }

(* Gestion des commentaires imbriqués *)
and comment i = parse
  
  | "(*"               { comment (i+1) lexbuf }
  | "*)"               { match i with 0 -> token lexbuf | _ -> comment (i-1) lexbuf }
  | _                  { comment i lexbuf }
