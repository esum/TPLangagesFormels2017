(** Type des expressions. *)
type t =
  | Var of string
  | Int of int
  | String of string
  | Let of string * t * t
  | App of string * t list
  | Case of t * (t * t) list
  | Any

(** Utilitaire pour afficher des listes. *)
let pp_list pp_item pp_sep chan l =
  let rec aux = function
    | [] -> ()
    | [e] -> pp_item chan e
    | hd::tl -> pp_item chan hd ; pp_sep chan ; aux tl
  in aux l

let pp_list2 pp_item pp_sep chan l =
  let pp_pair chan (e,e') =
    Format.fprintf chan "(%a,%a)" pp_item e pp_item e'
  in
  Format.fprintf chan "[%a]" (pp_list pp_pair pp_sep) l

(** Simplifieur d'expressions. *)
let rec simpl = function
  | Var v -> Var v
  | Int i -> Int i
  | String s -> String s
  | Let (x,v,b) -> Let (x,simpl v,simpl b)
  | App (f,args) -> (match args with
    | [a] -> (match f with
      | "-" -> (match simpl a with
        | Int i -> Int (-i)
        | _ -> App (f,List.map simpl args))
      | "not" -> (match simpl a with
        | Int i -> Int (match i with 0 -> 1 | _ -> 0)
        | _ -> App (f,List.map simpl args))
      | _ -> App (f,List.map simpl args))
    | [a; b] -> (match f with
      | "+" -> (match simpl a, simpl b with
        | Int i, Int j -> Int (i + j)
        | _ -> App (f,List.map simpl args))
      | "-" -> (match simpl a, simpl b with
        | Int i, Int j -> Int (i - j)
        | _ -> App (f,List.map simpl args))
      | "*" -> (match simpl a, simpl b with
        | Int i, Int j -> Int (i * j)
        | _ -> App (f,List.map simpl args))
      | "/" -> (match simpl a, simpl b with
        | Int i, Int j -> Int (i / j)
        | _ -> App (f,List.map simpl args))
      | "<" -> (match simpl a, simpl b with
        | Int i, Int j -> Int (match i < j with true -> 1 | false -> 0)
        | _ -> App (f,List.map simpl args))
      | "<=" -> (match simpl a, simpl b with
        | Int i, Int j -> Int (match i <= j with true -> 1 | false -> 0)
        | _ -> App (f,List.map simpl args))
      | ">" -> (match simpl a, simpl b with
        | Int i, Int j -> Int (match i > j with true -> 1 | false -> 0)
        | _ -> App (f,List.map simpl args))
      | ">=" -> (match simpl a, simpl b with
        | Int i, Int j -> Int (match i >= j with true -> 1 | false -> 0)
        | _ -> App (f,List.map simpl args))
      | "=" -> (match simpl a, simpl b with
        | Int i, Int j -> Int (match i = j with true -> 1 | false -> 0)
        | _ -> App (f,List.map simpl args))
      | "<>" -> (match simpl a, simpl b with
        | Int i, Int j -> Int (match i <> j with true -> 1 | false -> 0)
        | _ -> App (f,List.map simpl args))
      | "or" -> (match simpl a, simpl b with
        | Int i, Int j -> Int (match i <> 0 || j <> 0 with true -> 1 | false -> 0)
        | _ -> App (f,List.map simpl args))
      | "and" -> (match simpl a, simpl b with
        | Int i, Int j -> Int (match i <> 0 && j <> 0 with true -> 1 | false -> 0)
        | _ -> App (f,List.map simpl args))
      | _ -> App (f,List.map simpl args))
    | _ -> App (f,List.map simpl args))
  | Case (e,m) -> Case(e,List.map (function a, b -> simpl a, simpl b) m)
  | Any -> Any

(** Teste si la formule est close. *)
let clos =
  let rec clos env = function
    | Var v -> List.mem v env
    | Let (x,v,b) -> clos env v && clos (x::env) b
    | App (f,args) -> List.for_all (clos env) args
    | Case (e,m) -> clos env e && List.for_all (function a, b -> clos env a && clos env b) m
    | _ -> true in
  clos []


(** Afficheur d'expressions. *)
let pp_expr chan ast =
  let rec pp_expr chan = function
    | Var v -> Format.fprintf chan "%s" v
    | Int i -> Format.fprintf chan "%d" i
    | String s -> Format.fprintf chan "%s" s
    | Any -> Format.fprintf chan "?"
    | Let (x,v,b) ->
        Format.fprintf chan
          "@[<2>let %s = @,%a@, in @,%a@]" 
          x
          pp_expr v
          pp_expr b
    | App (f,args) ->
        Format.fprintf chan
          "%s(%a)"
          f
          (pp_list pp_expr (fun chan -> Format.fprintf chan ",@,")) args
    | Case(e,m) ->
        Format.fprintf chan
          "%s(%a,%a)"
          "case"
          pp_expr e
          (pp_list2 pp_expr (fun chan -> Format.fprintf chan ",@,")) m in
  pp_expr chan (simpl ast)
